# Deloitte Full Stack Developer Challenge

This project was generated with Angular CLI version 7.3.8.
 
The Application will allow the user to select a Regional Bloc and will list the names of countries belonging to it in the left column.
Clicking the country name will display the information on the right column using a static map.

You can view live demo [HERE](https://exam-237810.appspot.com/#)

## Prerequisites
* NPM must be installed
* Angular CLI must be installed to run & build locally
```
npm install -g @angular/cli
```


## Running in Localhost

* Use `ng serve`
```
ng serve --port <port> -- open
```

* Use `gulp`
```
gulp serve
```
Basically `gulp serve` will call `ng serve --port 4200 --open` by default.
__TODO: To be enhanced__

## Deployment
```
ng build
```
Compiles an Angular app into an output directory named `dist/` at the given output path. Must be executed from within a workspace directory. 

To test, navigate to `dist/`, use `lite-server` command to run.

To install lite-server
```
npm install -g lite-server
```

## Areas to be Improved
1. Back button support -- *To be clarified with Marvin*
2. Data Caching for country lists
3. Enhance design for Spinner 
4. Gulp file configuration
5. Load Regions & Languages list from API instead of hardcoding

## Authors

* **Marvin Mempin** - *Initial work*
* **Low Jian Sheng**
