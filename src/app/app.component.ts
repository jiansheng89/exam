import { Component } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [AppService]
})

export class AppComponent {
  private languages;
  private regions;
  private countryList;
  private loading = false;

  private selectedLanguage;
  private selectedRegion;
  private selectedCountry = {};

  constructor(private appService: AppService, private location: PlatformLocation) { }

  //init function
  ngOnInit() {

    this.location.onPopState((stateListener) => {
      //Functions when user click back
      if (!stateListener.state) {
        //Do nothing
      } else if (stateListener.state.navigationId) {
        //No more user state pushed, proceed to normal history back
        this.location.back();
      } else {
        //There is user state pop-ed, actions to display ccordingly
        this.selectedCountry = stateListener.state.selectedCountry;
        if (this.selectedRegion != stateListener.state.selectedRegion) {
          this.selectedRegion = stateListener.state.selectedRegion;
          this.updateList(false);
        }
      }
    });

    //Loading previous session from local storage (if any)
    this.selectedLanguage = localStorage.getItem('selectedLanguage');
    this.selectedRegion = localStorage.getItem('selectedRegion');

    //Initializing LOV
    this.getLanguages();
    this.getRegions();
  }

  getLanguages() {
    return this.appService.getLanguages().then(languages => {

      //Initialize default language if no previous session
      if (!this.selectedLanguage) {
        this.selectedLanguage = languages[0].code;
      }
      this.languages = languages;
    });
  }

  getRegions() {
    return this.appService.getRegions().then(regions => {

      this.regions = regions;

      //Initialize default region if no previous session
      if (!this.selectedRegion) {
        this.selectedRegion = regions[0].code;
      }

      //Initialize Country List
      this.updateList(true);
    });
  }

  updateList(defaultCountry) {

    this.loading = true; //Spinner on
    this.appService.getCountryList(this.selectedRegion)
      .subscribe((data) => {
        this.loading = false; //Spinner off
        this.countryList = this.translate(this.selectedLanguage, data); //Initialize country list language upon initialization
        if (defaultCountry) {
          this.loadCountryDetail(data[0]); // Default value is first country in list.
        } else {
          this.loadCountryDetail(this.selectedCountry); 
        }
        localStorage.setItem('selectedRegion', this.selectedRegion);
      });
  }

  loadCountryDetail(country) {
    //Update google map URL & update selected country to display
    country.googleMapURL = "https://maps.googleapis.com/maps/api/staticmap?center=" + country.latlng.join(",") + "&zoom=6&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:" + country.name.substring(0, 1) + "%7C" + country.latlng.join(",") + "&key=AIzaSyAkVIWjuD9Bg1uk1ZaUljB8wmjbwYX2udU";
    this.selectedCountry = country;

    //Update User click history for regooin & country
    let state = {
      selectedRegion: this.selectedRegion,
      selectedLanguage: this.selectedLanguage,
      selectedCountry: this.selectedCountry
    }
    this.location.pushState(state, null, "/#");
  }

  translate(language, data) {
    localStorage.setItem('selectedLanguage', language);
    return data.map(country => {
      if (language == 'EN') {
        country.displayName = country.name;
        // return country.displayName = country.name;
      } else {
        country.displayName = country.translations[language.toLowerCase()];
      }
      return country;
    })
  }

}
