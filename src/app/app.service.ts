import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

//Initialize constant variables
//TODO: Retrieve constant value from web service in case any future changes
const LANGUAGES = [
  { language: 'English', code: 'EN'},
  { language: 'German', code: 'DE' },
  { language: 'Spanish', code: 'ES' },
  { language: 'French', code: 'FR' },
  { language: '	Japanese', code: 'JA' }
]

//Added region type 'ALL' as default value
const REGIONS = [
  { region: 'All', code: 'ALL'},
  { region: 'European Union', code: 'EU' },
  { region: 'European Free Trade Association', code: 'EFTA' },
  { region: 'Caribbean Community', code: 'CARICOM' },
  { region: 'Pacific Alliance', code: 'PA' },
  { region: 'African Union', code: 'AU' },
  { region: 'Union of South American Nations', code: 'USAN' },
  { region: 'Eurasian Economic Union', code: 'EEU' },
  { region: 'Arab League', code: 'AL' },
  { region: 'Association of Southeast Asian Nations', code: 'ASEAN' },
  { region: 'Central American Integration System', code: 'CAIS' },
  { region: 'Central European Free Trade Agreement', code: 'CEFTA' },
  { region: 'North American Free Trade Agreement', code: 'NAFTA' },
  { region: 'South Asian Association for Regional Cooperation', code: 'SAARC' },
]

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http:HttpClient) { }

  getLanguages() {
    return new Promise(resolve => resolve(LANGUAGES));
  }

  getRegions() {
    return new Promise(resolve => resolve(REGIONS));
  }

  getCountryList(region){
    if (region == 'ALL'){
      return this.http.get("https://restcountries.eu/rest/v2/all")
    } else {
      return this.http.get("https://restcountries.eu/rest/v2/regionalbloc/" + region)
    }
  }
}
